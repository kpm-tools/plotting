"""Plotting functions and others."""

from collections import OrderedDict

import numpy as np
from scipy.sparse import diags
import matplotlib.pyplot as plt
from matplotlib.pylab import cm
from matplotlib.colors import ListedColormap
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d
import kwant


def plot_kwant(sites, site_color=None, linewidths=0.2, site_size=10,
               edgecolor='k', cmap='Blues', depth=False, elev=None,
               azim=None, ax=None, show=False, unit_cell=False,
               pos_transform=None, colorbar=True,
               **kwargs):
    """Plot a list of kwant system sites with site colors and colorbar."""
    if site_color is None:
        site_color = np.ones(len(sites))
    if unit_cell:
        family_name = sites[0].family.name
        sites = [s for s in sites if s.family.name == family_name]
        site_color = trace_unit_cell(sites, site_color)
        # traces norbs and sublattices
    else:
        sites = sites
        site_color = site_color
    try:
        assert len(sites) == site_color.shape[0]
    except Exception:
        raise ValueError("The number of sites (orbitals) to plot must match "
                         "the 'array' length")

    dim = sites[0].family.dim

    fig = plt.gcf()

    if pos_transform is None:
        def pos_transform(x):
            return x
    positions = np.array([pos_transform(s.pos) for s in sites]).T

    if site_color is None:
        site_color = np.ones(positions.shape[1])

    return_artists = True
    if ax is None:
        return_artists = False
        if dim == 3:
            ax = fig.add_subplot(111, projection='3d')
        else:
            ax = fig.add_subplot(111)

# TODO: check *positions warning
    if dim == 3:
        art = ax.scatter3D(
            *positions, c=site_color, cmap=cmap, linewidths=linewidths,
            s=site_size, edgecolor=edgecolor, depthshade=depth, **kwargs)

        ax.view_init(elev=elev, azim=azim)
    else:
        art = ax.scatter(
            *positions, c=site_color, cmap=cmap, linewidths=linewidths,
            s=site_size, edgecolor=edgecolor, **kwargs)

    if colorbar:
        fig.colorbar(art, ax=ax)

    if show:
        fig.show()

    if return_artists:
        return art
    return fig


def plot_neighbors(syst, lat=None, neighs=None, show=False, more_sites=False,
                   init_tags=[(0, 0)], ax=None,
                   scatter_kwargs={}, arrow_kwargs={}):
    """Plot arrows between the neighbors."""
    plot_kwargs = dict(color='k', arrowstyle='simple', mutation_scale=15, lw=0,
                       alpha=0.5)
    plot_kwargs.update(arrow_kwargs)
    dim = lat.sublattices[0].dim
    if neighs is None:
        if lat is None:
            try:
                lat = syst.sites[0].family
            except Exception:
                lat = list(syst.sites())[0].family
        neighs = []
        for sublat in lat.sublattices:
            neighs.extend(sublat.neighbors())

    fig = plt.gcf()
    if ax is None:
        ax = plt.gca()

    for init_tag in init_tags:
        for nei in neighs:
            site_a = nei.family_a(*init_tag)
            second_tag = np.array(init_tag) - np.array(nei.delta)
            site_b = nei.family_b(*(second_tag))
            delta_pos = (site_b.pos - site_a.pos)

            if dim == 2:
                ax.arrow(x=site_a.pos[0], y=site_a.pos[1],
                         dx=delta_pos[0], dy=delta_pos[1],
                         head_width=0.05, head_length=0.1, fc='k', ec='k')
            elif dim == 3:
                rs = np.array([site_a.pos, site_a.pos + delta_pos]).T
                arrow = Arrow3D(*rs, **plot_kwargs)
                ax.add_artist(arrow)

            ax.scatter(*site_a.pos, **scatter_kwargs)
            ax.scatter(*site_b.pos, **scatter_kwargs)

            if more_sites:
                site_a = nei.family_a(*second_tag)
                second_tag = second_tag - np.array(nei.delta)
                site_b = nei.family_b(*(second_tag))
                delta_pos = (site_b.pos - site_a.pos)

                if dim == 2:
                    ax.arrow(x=site_a.pos[0], y=site_a.pos[1],
                             dx=delta_pos[0], dy=delta_pos[1],
                             head_width=0.05, head_length=0.1, fc='k', ec='k')
                elif dim == 3:
                    rs = np.array([site_a.pos, site_a.pos + delta_pos]).T
                    arrow = Arrow3D(*rs, **plot_kwargs)
                    ax.add_artist(arrow)
                ax.scatter(*site_a.pos, **scatter_kwargs)
                ax.scatter(*site_b.pos, **scatter_kwargs)
    if show:
        plt.show()
    return ax


def plot_vectors(vectors, ax=None,
                 hl=0.1,  # head_length
                 head_width=0.05,
                 color='k',  # can be overridden,
                 show=False,
                 fig_size=None,
                 resize_ax=False,
                 margin=0.2,
                 ** kwargs
                 ):

    fig = plt.gcf()
    if fig_size is not None:
        fig.set_size_inches(fig_size)
    if ax is None:
        ax = plt.gca()

    directions = []
    for vector in vectors:
        _ec = vector.get('color', color)
        _fc = vector.get('color', color)
        _hl = vector.get('hl', hl)
        _hw = vector.get('head_width', head_width)
        origin = vector.get('origin', (0, 0))
        direction = vector.get('direction', (1, 1))
        directions.append(direction)

        ax.arrow(x=origin[0], y=origin[1],
                 dx=direction[0], dy=direction[1],
                 head_width=_hw,
                 head_length=_hl,
                 fc=_fc, ec=_ec,
                 **kwargs
                 )

    directions = np.array(directions)

    if resize_ax:
        ptp = np.ptp(directions, axis=0) * margin  # % margin
        lower_bound = np.min(directions, axis=0)
        upper_bound = np.max(directions, axis=0)

        ax.set_xlim(lower_bound[0] - ptp[0], upper_bound[0] + ptp[0])
        ax.set_ylim(lower_bound[1] - ptp[1], upper_bound[1] + ptp[1])
    if show:
        plt.show()
    return fig


class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
        FancyArrowPatch.draw(self, renderer)


def plot_arrows3D(start=None, arrow=None, ax=None, show=False, **kwargs):
    fig = plt.gcf()
    if ax is None:
        ax = fig.add_subplot(111, projection='3d')

    plot_kwargs = dict(mutation_scale=5, lw=3, arrowstyle="-|>", color="b",
                       alpha=0.5)
    plot_kwargs.update(kwargs)

    for i in range(len(start)):
        a = Arrow3D([start[i, 0], start[i, 0] + arrow[i, 0]],
                    [start[i, 1], start[i, 1] + arrow[i, 1]],
                    [start[i, 2], start[i, 2] + arrow[i, 2]],
                    **plot_kwargs)
        ax.add_artist(a)

    if show:
        plt.show()
    return fig


def identity_operator(x):
    """Return argument."""
    return x


def position_operator(syst, pos_transform=None):
    """Return a list of position operators as 'csr' matrices."""
    operators = []
    norbs = syst.sites[0].family.norbs
    if pos_transform is None:
        pos = np.array([s.pos for s in syst.sites])
    else:
        pos = np.array([pos_transform(s.pos) for s in syst.sites])

    for i in range(pos.shape[1]):
        operators.append(diags(np.repeat(pos[:, i], norbs), format='csr'))
    return operators


def trace_orbs(array, norbs=4, axis=-1):
    """Return the trace per unit-cell.

    'array' has a length 'num_sites * norbs' over the 'axis',
    and the return array has a length 'num_sites' over the
    same axis.
    """
    array = np.asarray(array)
    assert array.shape[axis] % norbs == 0

    slices = [[slice(None) for i in range(array.ndim)]
              for i in range(norbs)]
    for n in range(norbs):
        slices[n][axis] = slice(n, None, norbs)
        slices[n] = tuple(slices[n])
    return sum([array[slices[n]] for n in range(norbs)])


def trace_unit_cell(sites, array):
    """Trace the array assuming it has the values per orbital per site.

    If
    value_per_orbital_per_site
    """
    norbs = sites[0].family.norbs
    value_per_site = trace_orbs(array, norbs, axis=0)
    norbs = sites[0].family.norbs

    trace = OrderedDict()
    for s, value in zip(sites, value_per_site):
        _previous_value = trace.get(s.tag, 0)
        trace[s.tag] = _previous_value + value
    value_per_uc = np.array(list(trace.values()))

    return value_per_uc


def transparent_cmap(cmap=None, alpha='linear'):
    """Add transparency to a matplotlib ListedColormap.

    Parameters
    ----------
    cmap : instance of `matplotlib.pylab.cm`, optional
        The colormap to use.
    alpha : 'linear', float or array of floats
        If 'alpha' is a `float` or array of floats, this will be
        the transparency value. If it is 'linear' the transparency
        will decrease from '1' to '0'.

    """
    # Choose colormap
    if cmap is None:
        cmap = cm.viridis

    # Get the colormap colors
    my_cmap = cmap(np.arange(cmap.N))

    # Set alpha
    if alpha == 'linear':
        my_cmap[:, -1] = np.arange(cmap.N) / cmap.N
    elif isinstance(alpha, float):
        my_cmap[:, -1] = np.ones(cmap.N) * alpha
    else:
        my_cmap[:, -1] = alpha

    # Create new colormap
    return ListedColormap(my_cmap)


# Disorder distributions
def standard_gaussian(label, salt, s1, s2=None):
    """Return a gaussian distribution."""
    return kwant.digest.gauss(str(hash((s1, s2))), salt=salt + label)


def uniform(label, salt, s1, s2=None):
    """Return a uniform distribution in the interval '[-0.5, 0.5)'."""
    return kwant.digest.uniform(str(hash((s1, s2))), salt=salt + label) - 0.5


def expectation_value(vectors, operator):
    """Braket 'operator' with 'vectors'.

    'vectors' must have t1he last axis as the dimension to be multiplied
    by 'operator'
    """
    dim = vectors.shape[-1]
    new_shape = vectors.shape[:-1]
    vecs = vectors.reshape(-1, dim)
    output = np.empty(new_shape, dtype=complex).flatten()
    for i, vec in enumerate(vecs):
        output[i] = np.dot(vec.conj(), operator.dot(vec))

    output = output.reshape(new_shape)
    return output


def local_expectation_value(vector, operator, norbs=2):
    """Trace over sites."""
    assert len(vector.shape) == 1
    output = np.multiply(vector.conj(), operator.dot(vector))
    return np.real_if_close(trace_orbs(output, norbs=norbs))
